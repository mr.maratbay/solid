package Solid;

public class Main {
    public static void main(String[] args){

        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Abai"));
        company.addEmployee(new JavascriptDeveloper("Marat"));
        company.addEmployee(new FullStackDeveloper("Dias"));
        company.startWork();
    }
}

